﻿using slqLocalDbTest.Model;
using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Media;

namespace slqLocalDbTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        /// <summary>
        /// test
        /// </summary>
        private void CheckSqlLocalDbStatus()
        {
            try
            {
                string content = LocalDBOperationManager.CheckSqlLocalDbStatus();
                lblstatus.Text = string.Empty;
                string[] rows = Regex.Split(content, "\r\n");
                foreach (string row in rows)
                {
                    lblstatus.Text += row;
                    string[] tokens = Regex.Split(row, "\\s+");
                    if ((tokens[0].Equals("State:")))
                    {
                        string status = Convert.ToString(tokens[1]);
                        if (status.Equals("Running"))
                        {
                            lblName.Foreground = Brushes.White;
                            lblName.Background = Brushes.Green;
                        }
                        else if (status.Equals("Stopped"))
                        {
                            lblName.Foreground = Brushes.White;
                            lblName.Background = Brushes.Red;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Unable to check SQLLocalDb status", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void StartSqlLocalDbStatus()
        {
            try
            {
                string content = LocalDBOperationManager.StartSqlLocalDbStatus();
                lblstatus.Text = string.Empty;
                string[] rows = Regex.Split(content, "\r\n");
                foreach (string row in rows)
                {
                    lblstatus.Text += row;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Unable to start", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Stop local db instance.
        /// </summary>
        private void StopSqlLocalDbStatus()
        {
            try
            {
                string content = LocalDBOperationManager.StopSqlLocalDbStatus();
                lblstatus.Text = string.Empty;
                string[] rows = Regex.Split(content, "\r\n");
                foreach (string row in rows)
                {
                    lblstatus.Text += row;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Unable to stop", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Button_Check_Click(object sender, RoutedEventArgs e)
        {
            CheckSqlLocalDbStatus();
        }

        private void Button_Start_Click(object sender, RoutedEventArgs e)
        {
            lblName.Foreground = Brushes.Black;
            lblName.Background = Brushes.Transparent;
            StartSqlLocalDbStatus();
        }

        private void Button_Stop_Click(object sender, RoutedEventArgs e)
        {
            lblName.Foreground = Brushes.Black;
            lblName.Background = Brushes.Transparent;
            StopSqlLocalDbStatus();
        }
    }
}
