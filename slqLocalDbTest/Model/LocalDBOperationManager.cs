﻿using System;
using System.Diagnostics;
using System.IO;

namespace slqLocalDbTest.Model
{
    class LocalDBOperationManager
    {
        /// <summary>
        /// test
        /// </summary>
        public static string CheckSqlLocalDbStatus()
        {
            string content = string.Empty;
            try
            {
                using (Process process = new Process())
                {
                    ProcessStartInfo startInfoSqllocalDb = new ProcessStartInfo();
                    startInfoSqllocalDb.FileName = "sqllocaldb.exe";
                    startInfoSqllocalDb.Arguments = "info v11.0";
                    startInfoSqllocalDb.UseShellExecute = false;
                    startInfoSqllocalDb.RedirectStandardOutput = true;
                    startInfoSqllocalDb.RedirectStandardError = true;
                    // Do not create the black window.
                    startInfoSqllocalDb.CreateNoWindow = true;
                    startInfoSqllocalDb.ErrorDialog = false;
                    startInfoSqllocalDb.WindowStyle = ProcessWindowStyle.Hidden;

                    process.StartInfo = startInfoSqllocalDb;
                    process.Start();

                    StreamReader stdOutput = process.StandardOutput;
                    StreamReader stdError = process.StandardError;

                    content = String.Format("{0}\r\n{1}", stdOutput.ReadToEnd(), stdError.ReadToEnd());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return content;
        }

        public static string StartSqlLocalDbStatus()
        {
            string content = string.Empty;
            try
            {
                using (Process process = new Process())
                {
                    ProcessStartInfo startInfoSqllocalDb = new ProcessStartInfo();
                    startInfoSqllocalDb.FileName = "sqllocaldb.exe";
                    startInfoSqllocalDb.Arguments = "start v11.0";
                    startInfoSqllocalDb.UseShellExecute = false;
                    startInfoSqllocalDb.RedirectStandardOutput = true;
                    startInfoSqllocalDb.RedirectStandardError = true;
                    // Do not create the black window.
                    startInfoSqllocalDb.CreateNoWindow = true;
                    startInfoSqllocalDb.ErrorDialog = false;
                    startInfoSqllocalDb.WindowStyle = ProcessWindowStyle.Hidden;

                    process.StartInfo = startInfoSqllocalDb;
                    process.Start();

                    StreamReader stdOutput = process.StandardOutput;
                    StreamReader stdError = process.StandardError;

                    content = stdOutput.ReadToEnd() + stdError.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return content;
        }

        public static string StopSqlLocalDbStatus()
        {
            string content = string.Empty;
            try
            {
                using (Process process = new Process())
                {
                    ProcessStartInfo startInfoSqllocalDb = new ProcessStartInfo();
                    startInfoSqllocalDb.FileName = "sqllocaldb.exe";
                    startInfoSqllocalDb.Arguments = "stop v11.0";
                    startInfoSqllocalDb.UseShellExecute = false;
                    startInfoSqllocalDb.RedirectStandardOutput = true;
                    startInfoSqllocalDb.RedirectStandardError = true;
                    // Do not create the black window.
                    startInfoSqllocalDb.CreateNoWindow = true;
                    startInfoSqllocalDb.ErrorDialog = false;
                    startInfoSqllocalDb.WindowStyle = ProcessWindowStyle.Hidden;

                    process.StartInfo = startInfoSqllocalDb;
                    process.Start();

                    StreamReader stdOutput = process.StandardOutput;
                    StreamReader stdError = process.StandardError;

                    content = stdOutput.ReadToEnd() + stdError.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return content;
        }

    }
}
